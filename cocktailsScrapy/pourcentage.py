import json
from pprint import pprint

with open("www/data/1001cocktails_output.json", 'r+') as data_file:    
	data = json.load(data_file)

	for cocktail in data:
		for ingredient in cocktail["ingredients"]:
			if ingredient["dosage"] == 0.0 : 
				print ("Pas d'ingredient")
			else :	
				x = 0.0
				x = float(ingredient["dosage"])*100
				ingredient["dosage"]  = '%.2f' % x
				print (repr(ingredient["dosage"]))
			
	data_file.seek(0)
	json.dump(data, data_file, indent = 4)



	