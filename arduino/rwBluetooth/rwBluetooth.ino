#include <SoftwareSerial.h>
#include<stdlib.h>

SoftwareSerial BT(10, 11); 
// creates a "virtual" serial port/UART
// connect BT module TX to D10
// connect BT module RX to D11
// connect BT Vcc to 5V, GND to GND

float dosageMax = 0;
float dosageActuel = 0;
boolean lancement = false;
boolean fin = false;
String command = "";

void setup()  
{
  Serial.begin(9600);
  // set digital pin to control as an output
  pinMode(13, OUTPUT);
  // set the data rate for the SoftwareSerial port
  BT.begin(9600);
  // Send test message to other device
  Serial.println("Hello from Arduino");
}

void loop() 
{
  // Read device output if available.
  if (BT.available()) {
    
    command = BT.readStringUntil('\n');
    Serial.println(command);

    if(command.length() > 0) 
    {
      BT.println(command);
      
      if (command.equals("lancement"))
      {
        BT.write("lancement_ok");
        Serial.println("lancement_ok");
        lancement = true;
        BT.write(0xA);
      }
   
      if (lancement == false) {
        if(command.indexOf("dosageMax") >= 0) 
        {
          String commandT = command.substring(8);
          dosageMax = commandT.substring(9).toFloat();  
          Serial.println("Dosage max : " + commandT.substring(9));
        }
        if (dosageActuel < dosageMax)
        {
          if (command.equals("dosage"))
          {
            dosageActuel++;
            delay(500);
            String txt = String(dosageActuel);
            BT.write(txt.c_str ());
            BT.write(0xA);
            Serial.println(txt);
          }
        } else {
          BT.write("fin_ingredient");
          BT.write(0xA);
          dosageActuel = 0;
        }
        if (command.equals("fin"))
          {
            BT.write("fin_ok");
            BT.write(0xA);
          }
      }
  
      if (lancement == true)
      {
        lancement = false;
      } 
      
      command = ""; // No repeats
    }
  }
  
}
