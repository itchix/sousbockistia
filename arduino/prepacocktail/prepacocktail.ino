 #include <SoftwareSerial.h>
#include <stdlib.h>
#include <HX711.h>

// Hx711.DOUT - pin #A2
// Hx711.SCK - pin #A3
HX711 scale(A2, A3);

SoftwareSerial BT(10, 11); 
// creates a "virtual" serial port/UART
// connect BT module TX to D10
// connect BT module RX to D11
// connect BT Vcc to 5V, GND to GND

float masseCocktail = 0;
float masseActuelle = 0;//à lire sur A0
float dosageIngredient = 0;
boolean lancement = false;
boolean fin = false;
String command = "";

void setup()  
{
  Serial.begin(9600);
  // set digital pin to control as an output
  pinMode(13, OUTPUT);
  // set the data rate for the SoftwareSerial port
  BT.begin(9600);
  
  scale.set_scale(2000);
  scale.tare();
  
  // Send test message to other device
  Serial.println("Hello from Arduino");
}

void loop() 
{
  Serial.print("Value :\t");
  Serial.println(scale.get_units(5), 1);

  // Read device output if available.
  if (BT.available()) {
    
    command = BT.readStringUntil('\n');
    Serial.println("Recu : " + command);
    
    if(command.length() > 0) 
    {
      if(command.equals("init")) 
      {
        float masseCocktail = 0;
        float masseActuelle = 0;//à lire sur A0
        float dosageIngredient = 0;
        int nbI = 0;
        int index_t = 0;
        boolean lancement = false;
        boolean fin = false;
        String command = "";
        
        scale.set_scale(2000);
        scale.tare();
      }
      if (command.indexOf("masseCocktail") >= 0)
      {
        masseCocktail = command.substring(13).toFloat();
        BT.write("Masse_reçue");
        Serial.println("Masse_reçue");
        lancement=true;
        BT.write(0xA);
      }

   
      if (lancement == false) {
        if(command.indexOf("dosageIngredient") >= 0) 
        {
          scale.tare();
          dosageIngredient = command.substring(16).toFloat();
          masseActuelle = scale.get_units(10);
          Serial.println("Dosageingredient_recu");
          BT.write("Dosageingredient_recu");
          BT.write(0xA);
        }
        if(command.equals("masseActuelle"))
        {
          masseActuelle = scale.get_units(10);
          String masseS =  String(masseActuelle, 2);
          String masseString =  String("masse " + masseS);   
          BT.write(masseString.c_str());
          BT.write(0xA);
        }
        
        if ((masseActuelle/masseCocktail) >= (dosageIngredient/100))
        {
          BT.write("Stop_ingredient");
          Serial.println("Stop_ingredient");
          BT.write(0xA);
          masseActuelle = 0;
          scale.tare();
        }
        if (command.equals("Cocktailtermine"))
          {
            BT.write("Cocktailtermine_ok");
            BT.write(0xA);
            Serial.println("Cocktailtermine_ok");
          }
      }
  
      if (lancement == true)
      {
        lancement = false;
      } 

      scale.power_down();
      delay(100);
      scale.power_up();
      
      command = ""; // No repeats
    }
  }
  
}

