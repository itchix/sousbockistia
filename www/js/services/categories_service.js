angular.module('starter.services')

/**
* Categories factory
* Permet la gestion du modèle categorie
* $params : $localstorage
**/
.factory('Categories', function($localstorage){

  var localCategories = $localstorage.getObject('categories');

  return {
    /**
    * Récupère toutes les categories
    * $params : /
    * return : localCategories (tab de categories)
    **/
    allCategories: function() {
      return localCategories;
    },
    /**
    * Récupère une categorie
    * $params : categorieId
    * return : localCategories[i]
    **/
    getCat: function(categorieId) {
      for (var i = 0; i < localCategories.length; i++) {
        if (localCategories[i].idCategorie === parseInt(categorieId)) {
          return localCategories[i];
        }
      }
      return null;
    }
  };
});