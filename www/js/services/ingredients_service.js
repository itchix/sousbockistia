angular.module('starter.services')

.factory('Camera', ['$q', function($q) {
 
  return {
    getPicture: function(options) {
      var q = $q.defer();
      
      navigator.camera.getPicture(function(result) {
        // Do any magic you need
        q.resolve(result);
      }, function(err) {
        q.reject(err);
      }, options);
      
      return q.promise;
    }
  }
}])
/**
* Ingrédients factory
* Permet la gestion du modèle ingrédient
* $params : $localstorage
**/
.factory('Ingredients', function($localstorage) {

  var localCocktails = $localstorage.getObject('cocktails');

  return {
    /**
    * Retourne tous les ingrédients des cocktails
    * $param : /
    * return : /
    **/
    all: function() {
      var ingredientTemp = new Array();
      var add = 1;    

      ingredientTemp.push(localCocktails[0].ingredients[0].nom);

      for(var i = 0; i < localCocktails.length; i++) {
        for (var k = 0; k < localCocktails[i].ingredients.length; k++) {
        add = 1;   
         for(var j = 0; j < ingredientTemp.length; j++) {
           if(ingredientTemp[j] == localCocktails[i].ingredients[k].nom) {
             add = 0;
            } 
          }
          if(add == 1) {
            ingredientTemp.push(localCocktails[i].ingredients[k].nom);            
          }
        }    
      }
      return ingredientTemp;
    },

    getId: function()
    {
      return (localCocktails.length + 1);
    },

    /**
    * Retourne tous les cocktails avec les ingredients
    * $param : ingredients
    * return : /
    **/
    getCocktailsByIngredients: function(ingredients) {
      var ok = false;
      var temp = 0;
      var cocktailsTemp = new Array();
      
      for(var i = 0; i < localCocktails.length; i++) {
        for (var k=0; k < localCocktails[i].ingredients.length; k ++) {   
          for(var j = 0; j < ingredients.length; j++) {
            if(ingredients[j].nom == localCocktails[i].ingredients[k].nom) {
              ok = true;
              temp++;
            } 
          }
        } 
        if(ok && temp == ingredients.length) {
          cocktailsTemp.push(localCocktails[i]);            
        }   
        temp = 0;
        ok = false;
      }
      return cocktailsTemp;
    },

    /**
    * Retourne tous les ingrédients des cocktails en fonction du string en param
    * $param : /
    * return ingredientTemp
    **/
   getIngredients: function(str) {
      
      var ingredientTemp = new Array();
      var add;  

      if(str == "")
      {
        return 0;
      }
      if(localCocktails[0].ingredients[0].nom.indexOf(str)>0)
      {
        ingredientTemp.push(localCocktails[0].ingredients[0].nom);
      }     
      
      for(var i = 0; i < localCocktails.length; i++) {
        add = 1;
        for (var k=0; k < localCocktails[i].ingredients.length; k ++)
        {
           
           for(var j = 0; j < ingredientTemp.length; j++) {
             if(ingredientTemp[j] == localCocktails[i].ingredients[k].nom) {
               add = 0;
              } 
            }

            if(add == 1) {
              if(localCocktails[i].ingredients[k].nom.indexOf(str)>=0)
              {
                ingredientTemp.push(localCocktails[i].ingredients[k].nom);
              }              
            }
        }    
     }
     return ingredientTemp;
    }
  };

});