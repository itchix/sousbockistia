angular.module('starter.services')


/**
* Cocktails factory
* Permet la gestion du modèle cocktails
* $params : $localstorage
**/
.factory('Cocktails', function($localstorage) {

  var localCocktails = $localstorage.getObject('cocktails');

  return {
    /**
    * Récupère tous les cocktails
    * $params : /
    * return : cocktailsTemp
    **/
    all: function() {
      var cocktailsTemp = localCocktails;
      return cocktailsTemp;
    },
    /**
    * Récupère tous les cocktails en favoris
    * $params : /
    * return : cocktailsTemp
    **/
    allfavourite: function() {
      var cocktailsTemp = new Array();
      for (var i = 0; i < localCocktails.length; i++) {
        if (localCocktails[i].favoris == 1) {
          cocktailsTemp.push(localCocktails[i]);
        }
      }
      return cocktailsTemp;
    },

    /**
    * Supprime un cocktail
    * $params : object cocktail
    * return : /
    **/
    remove: function(cocktail) {
      localCocktails.splice(localCocktails.indexOf(cocktail), 1);
    },
    /**
    * Recupère tous les cocktails d'une categorie
    * $params : int categorie
    * return : cocktailsTemp
    **/
    getfromcategorie: function(categorie)
    {
      var cocktailsTemp = new Array();

      for(var i = 0; i < localCocktails.length; i++) {
        if(parseInt(localCocktails[i].categorie) == parseInt(categorie)) {
          cocktailsTemp.push(localCocktails[i]);
        }
      }
      return cocktailsTemp;
    },
    /**
    * Enregistre un cockatil
    * $params : int cocktailId
    * return : localCocktails[i]
    **/
    save: function(cocktailId) {
      for(var i = 0; i < localCocktails.length; i++) {
          if (localCocktails[i].idCocktail === parseInt(cocktailId)) {
            if (localCocktails[i].favoris === 1) {
              localCocktails[i].favoris = 0;
            }
            else {
              localCocktails[i].favoris = 1; 
            }
            $localstorage.setObject('cocktails', localCocktails);
          }
      }
    },

    add: function(cocktail) {

      localCocktails.push(cocktail);
      $localstorage.setObject('cocktails', localCocktails);      
    },
    /**
    * Récupère un cockatil
    * $params : cocktailId
    * return : localCocktails[i]
    **/
    get: function(cocktailId) {
      for (var i = 0; i < localCocktails.length; i++) {
        if (localCocktails[i].idCocktail === parseInt(cocktailId)) {
          return localCocktails[i];
        }
      }
      return null;
    }
  };

});