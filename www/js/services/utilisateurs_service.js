angular.module('starter.services')

/**
* Utilisateur factory
* Permet la gestion du modèle utilisateur
* $params : $localstorage
**/
.factory('Utilisateurs', function($localstorage) {

  var localUtilisateurs = $localstorage.getObject('utilisateurs');

  return {
    /**
    * Récupère tous les utilisateurs
    * $params : /
    * return : localUtilisateurs
    **/
    all: function() {
      return localUtilisateurs;
    },
    /**
    * Supprime un utilisateur
    * $params : utilisateur
    * return : /
    **/
    remove: function(utilisateur) {
      localUtilisateurs.splice(localUtilisateurs.indexOf(chat), 1);
    },
    /**
    * Récupère un utilisateur
    * $params : utilisateur
    * return : localUtilisateur[i]
    **/
    get: function(utilisateurId) {
      for (var i = 0; i < localUtilisateurs.length; i++) {
        if (localUtilisateurs[i].id === parseInt(utilisateurId)) {
          return localUtilisateurs[i];
        }
      }
      return null;
    }
  };

});