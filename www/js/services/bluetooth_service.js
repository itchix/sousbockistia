angular.module('starter.services')

/**
* Ingrédients factory
* Permet la gestion du modèle bluetooth
* $params : $localstorage
**/
.factory('Bluetooth', function($localstorage) {

  return {
    /**
    * Enregistre un appareil
    * $params : bluetooth
    * return : /
    **/
    save: function(bluetooth) {
      $localstorage.setObject('bluetooth', bluetooth);
    },
    /**
    * Récupère un appareil
    * return : localBluetooth
    **/
    get: function() {
      return $localstorage.getObject('bluetooth');
    }
  };

});