angular.module('starter.filters', [])

.filter('newlines', function () {
  return function(text) {
      return text.replace(/•/g, '<br/> •');
  }
})

.filter('pathimg', function() {
  return function(text) {
      if(text != null) {
        return text.replace(/full/g, 'img/full');
      } 
  }
})

.filter('pathimgcateg', function() {
  return function(text) {
    text = text.replace(/é/g, 'e');
    text = text.replace(/è/g, 'e');
    text = text.replace(/à/g, 'a');
    text = text.replace(/ù/g, 'u');
    return text;
  }
})

.filter('nomIngredient', function() {
  return function(text) {
    if (text.indexOf("(")!= -1){
      text = text.substring(0,text.indexOf("(")) + "*";
    }
    return text;
  }
})

.filter('majFirstLetter', function() {
  return function(text) {
    return text.charAt(0).toUpperCase() + text.slice(1);
  }
})

.filter('dosage', function() {
  return function(text) {
    if (text == " ") {
      return "(Completer à votre guise)";
    } else {
      text = text.substring(0,text.indexOf("."));
      if (text.length==1){ text = 2; 
      } else {
        if (text.substring(1) >6 ) {
          var i = (parseInt(text.substring(0,1)) +1) *2;
          text = i.toString();
        } else if (text.substring(1) >= 4 && text.substring(1) <= 6)
        {
          var i = (parseInt(text.substring(0,1)) *2 ) +1;
          text = i.toString();
        } else {
           var i = (parseInt(text.substring(0,1))) * 2;
          text = i.toString();
        }
      }
      return text + "/20";
    }
  }  
})

.filter('afficherDosage', function(Cocktails) {
  return function(text,id) {
    
    if(text == ' ')
    {
      text = "À votre guise";
    } else
    {
      var taille = Cocktails.get(id).ingredients.length;
      var dosage = 0;    
      var masse = Cocktails.get(id).masse;      
      //return dosage;
      if(text == Cocktails.get(id).ingredients[taille -1].dosage) 
      {
        var masseActuelle = 0;
        dosage = 0;
        for(var i =0; i<taille-1;i++)
        {
          masseActuelle = parseFloat(Cocktails.get(id).ingredients[i].dosage);
          masseActuelle = (masseActuelle/100)*masse;
          if((masseActuelle +0.5) > parseInt(masseActuelle)+1)
          {
            masseActuelle = parseInt(masseActuelle) + 1;
          }else
          {
            masseActuelle = parseInt(masseActuelle);
          } 
          dosage = dosage + masseActuelle;
        }
      dosage = masse - dosage;
      var volume = parseInt(masse) / 10;
     // dosage = dosage /10;
      text = dosage + "/" + volume + "cl";
      }else
      {
        dosage = parseFloat(text);                
        dosage = (dosage/100) * masse;
        if((dosage +0.5) > parseInt(dosage)+1)
        {
          dosage = parseInt(dosage) + 1;
        }else
        {
          dosage = parseInt(dosage);
        } 
      }
      var volume = parseInt(masse) / 10;
      dosage = dosage /10;
      text = dosage + "/" + volume + "cl";
     }

    return text;
    
  }  
});