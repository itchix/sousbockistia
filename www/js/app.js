// Ionic Starter App
angular.module('starter.controllers', []);
angular.module('starter.services', []);
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.filters','starter.services', 'ionic.utils', 'ngCordova', 'ionic-modal-select', 'angular-svg-round-progressbar'])

.run(function($ionicPlatform, $rootScope, $state, $window, $q, $cordovaSplashscreen, $http, $ionicPopup, $localstorage) {

  $ionicPlatform.on('pause', function() {
    bluetoothSerial.disconnect();
  });

  $ionicPlatform.on('resume', function() {
    
  });

  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    // Save in localstorage when it's the first use of the app
    if(!$localstorage.get('firstUse')) {

      $localstorage.set('firstUse', 1);
      $localstorage.setObject('bluetooth', "");
      var url = "";
      if(ionic.Platform.isAndroid()){
        url = "/android_asset/www/";
      }
      this.cocktailsJson = $http.get(url+'data/1001cocktails_output.json').success(
        function(response){ 
          $localstorage.setObject('cocktails', response);
        }, function(response) {
          console.error('ERROR localstorage', response);
          $localstorage.set('firstUse', 0);
        }
      );
      this.categoriesJson = $http.get(url+'data/categories_data.json').success(
        function(response){ 
          $localstorage.setObject('categories', response);
        }, function(response) {
          console.error('ERROR localstorage', response);
          $localstorage.set('firstUse', 0);
        }
      );

      $q.all([this.cocktailsJson, this.categoriesJson])
      .then(function(values) {
        $window.location.reload(true);
      });
    } else {
      if (navigator.splashscreen) {
        navigator.splashscreen.hide();
      } 
    }

  });

  // Display back button
  $rootScope.goBack = function() {
        // function to go back
        window.history.back();
    }


})

.config(function($stateProvider, $ionicConfigProvider, $urlRouterProvider) {

  // Tabs en bas pour Android
  $ionicConfigProvider.tabs.position('bottom');
  $ionicConfigProvider.views.transition('none');

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
  .state('menu', {
    url: '/menu',
    abstract: true,
    templateUrl: 'templates/menu-abstract.html',
    controller: 'MenuCtrl'
  })

  .state('menu.home', {
    url: '/home',
    views: {
      'view-content': {
        templateUrl: 'templates/home.html',
        controller: 'HomeCtrl'
      }
    }
  })

  .state('menu.categories', {
    url: '/categories',
    views: {
      'view-content': {
        templateUrl: 'templates/categories.html',
        controller: 'CategoriesCtrl'
      }
    }
  })


  .state('menu.ingredient', {
    url: '/ingredients',
    views: {
      'view-content': {
        templateUrl: 'templates/cocktail-ingredient.html',
        controller: 'RechercheIngredientCtrl'
      }
    }
  }) 

  .state('menu.cocktailCategorie', {
      url: '/cocktailCategorie/:idCategorie',
     views: {
      'view-content': {
   
    templateUrl: 'templates/cocktail-categorie.html',
    controller: 'CocktailsCategorieCtrl'
  }}
  })


  .state('menu.cocktails', {
    url: '/cocktails',
    views: {
      'view-content': {
        templateUrl: 'templates/cocktails.html',
        controller: 'CocktailsCtrl'
      }
    }
  })

  .state('menu.favourite', {
    url: '/favourite',
    views: {
      'view-content': {
        templateUrl: 'templates/favourite.html',
        controller: 'FavouriteCtrl'
      }
    }
  })

  .state('menu.cocktail', {
    url: '/cocktail/:id',
     views: {
      'view-content': {
    templateUrl: 'templates/cocktail-detail.html',
    controller: 'CocktailDetailsCtrl'
  }}
  })

  .state('menu.creerCocktail', {
    url: '/creer-cocktail',
     views: {
      'view-content': {
        templateUrl: 'templates/creer-cocktail.html',
        controller: 'CreerCtrl'
      }
    }
  })

  .state('menu.preparerCocktail', {
    url: '/preparer-cocktail/:id',
     views: {
      'view-content': {
    templateUrl: 'templates/preparer-cocktail.html',
    controller: 'PreparerCtrl',
    cache: false
  }}
  })

  .state('menu.settings', {
    url: '/settings',
    views: {
      'view-content': {
        templateUrl: 'templates/settings.html',
        controller: 'SettingsCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/menu/home');
});
