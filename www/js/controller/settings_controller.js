angular.module('starter.controllers')

.controller('SettingsCtrl', function($scope, $ionicPopup, $cordovaToast, Bluetooth, $ionicLoading){
  
  $scope.bluePeriph = Bluetooth.get().name;
  
  document.addEventListener("deviceready", function () {

    $ionicLoading.show({
      template: '<ion-spinner icon="lines"></ion-spinner></br>Chargement...',
      content: 'Chargement',
      animation: 'fade-in',
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0
    });

    $scope.saveMacBluetooth = function(newValue, oldValue) {
      Bluetooth.save(newValue);
      $scope.bluePeriph = Bluetooth.get().name;
      $ionicPopup.alert({
        title: 'Bluetooth',
        template: 'Sous-bock enregistré. (' + newValue.name + ')'
      });
    };

    bluetoothSerial.list(function(response) {$ionicLoading.hide(); $scope.list = response}, function(response) {$ionicLoading.hide();});

  }, false);

});