angular.module('starter.controllers')

/**
* Favourite controller
* return : 
*   - $scope.cocktail : recupere le tous les cocktails
*   - $scope.numberOfItemsToDisplay : nombre de cocktails à afficher
**/
.controller('FavouriteCtrl', function($scope, Cocktails, $ionicLoading, $timeout) {
  $scope.$on('$ionicView.enter', function(){
    $ionicLoading.show({
      template: '<ion-spinner icon="lines"></ion-spinner></br>Chargement...',
      content: 'Chargement',
      animation: 'fade-in',
      showBackdrop: true,
      maxWidth: 200,
      showDelay: 0
    });

    $timeout(function () {
      $ionicLoading.hide();
      $scope.cocktails = Cocktails.allfavourite();
    }, 500); 

    $scope.cocktails = [];
    $scope.numberOfItemsToDisplay = 20;
    $scope.clearSearch = function() {
      this.searchFilter.nom = "";
    } 
  });

  // ScrollInfinite : charge 10 cocktails de plus a chaque fois que l'on scroll la page
  $scope.addMoreItem = function() {
    if($scope.cocktails){
      if ($scope.cocktails.length > $scope.numberOfItemsToDisplay) {
        $scope.numberOfItemsToDisplay += 10; 
        $scope.$broadcast('scroll.infiniteScrollComplete');
      }
    }
    $scope.$broadcast('scroll.infiniteScrollComplete');
  }
})