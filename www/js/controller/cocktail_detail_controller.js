angular.module('starter.controllers')

/**
* Cocktails Details controller
* return : 
*   - $scope.cocktail : recupere le cocktail
*   - $scope.image : recupere l'image du cocktail
**/
.controller('CocktailDetailsCtrl', function($scope, $stateParams, Cocktails, $ionicPopup, $ionicPopover, $ionicHistory) {
  $scope.cocktail = Cocktails.get($stateParams.id);
  $scope.image = (Cocktails.get($stateParams.id).images[0].path).replace(/full/g, 'img/full');

  var popo;

  $scope.$watch('cocktail.favoris',function(newVal,oldVal){
    if (newVal === 0) {
      $scope.texte = 'Mettre en favoris';
      document.getElementById("favorisButton").className = "button ion-ios-star-outline button-positive icon-left";
    }
    else {
      $scope.texte = 'Oter des favoris';
      document.getElementById("favorisButton").className = "button ion-ios-star button-positive icon-left";
    }
  })
  
  $scope.fav = function() {
    Cocktails.save($stateParams.id);
    // An alert dialog
    if ($scope.cocktail.favoris === 0) {
      var alertPopup = $ionicPopup.alert({
      title: 'Oh non...',
      template: 'Vous avez retiré un cocktail de vos favoris'
      });
    }
    else { 
      var alertPopup = $ionicPopup.alert({
      title: 'Bravo !',
      template: 'Vous avez ajouté un cocktail à vos favoris'
      });
    }
  }


  $scope.$on('$ionicView.leave', function(){
    $ionicHistory.clearCache();
  });

  $scope.detail = function(nom) {
    console.log(nom);
    var det = nom;
    if (nom.indexOf("(")!= -1){  
      det = det.slice(det.indexOf("(")+1,det.indexOf(")"));
      det = det.charAt(0).toUpperCase() + det.slice(1);

      nom = nom.substring(0,nom.indexOf("("));
      nom = nom.charAt(0).toUpperCase() + nom.slice(1);

      //var template = '<style>.popover { height:100px; width: 200px; }</style><ion-popover-view><ion-header-bar> <h1 class="title"> Détails '
      //+ nom + '</h1> </ion-header-bar> <ion-content> <center><b>' + det+ '</b></center> </ion-content></ion-popover-view>';

      //$scope.popover = $ionicPopover.fromTemplate(template, {scope: $scope});
      //popo = document.getElementById('popover');
      //console.log(popo);
      //$scope.popover.show(popo);
      }
   }
});