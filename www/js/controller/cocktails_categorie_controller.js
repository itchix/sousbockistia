angular.module('starter.controllers')

/**
* Categorie controller
* return : 
*   - $scope.cocktails : recupere les cocktails en fonction de la categorie en question
*   - $scope.numberOfItemsToDisplay : nombre de cocktails à afficher
**/
.controller('CocktailsCategorieCtrl', function($scope, $stateParams, Cocktails, Categories, $ionicLoading, $timeout) {
  $ionicLoading.show({
    template: '<ion-spinner icon="lines"></ion-spinner></br>Chargement...',
    content: 'Chargement',
    animation: 'fade-in',
    showBackdrop: true,
    maxWidth: 200,
    showDelay: 0
  });

  $timeout(function () {
    $ionicLoading.hide();
    $scope.cocktails = Cocktails.getfromcategorie($stateParams.idCategorie);
  }, 500);  

  $scope.cocktails = [];
  $scope.categorie = Categories.getCat($stateParams.idCategorie);
  $scope.numberOfItemsToDisplay = 20;
  $scope.clearSearch = function() {
    this.searchFilter.nom = "";
  } 

  // ScrollInfinite : charge 10 cocktails de plus a chaque fois que l'on scroll la page
  $scope.addMoreItem = function() {
    if($scope.cocktails){
      if ($scope.cocktails.length > $scope.numberOfItemsToDisplay) {
        $scope.numberOfItemsToDisplay += 10; 
        $scope.$broadcast('scroll.infiniteScrollComplete');
      }
    }
    $scope.$broadcast('scroll.infiniteScrollComplete');
  }
});