angular.module('starter.controllers')

.controller('MenuCtrl', function($scope, $ionicSideMenuDelegate, $timeout, Cocktails) {
	$scope.toggleLeft = function() {
		$ionicSideMenuDelegate.toggleLeft();
	};

	var cocktailsTemp = Cocktails.all();
	$scope.i = 0
	function update30sec() {
    	$timeout(update30sec , 10000);
    	$scope.i++;
	};
	update30sec();

	$scope.$watch('i',function(newVal,oldVal){
		$scope.cocktail = cocktailsTemp[Math.floor(Math.random()*cocktailsTemp.length)];
	});
});