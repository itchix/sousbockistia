angular.module('starter.controllers')

/**
* Cocktails controller
* return : 
*   - $scope.cocktails : recupere les cocktails
*   - $scope.numberOfItemsToDisplay : nombre de cocktails à afficher
**/
.controller('CocktailsCtrl', function($scope, Cocktails, $ionicLoading, $timeout) {
  $ionicLoading.show({
    template: '<ion-spinner icon="lines"></ion-spinner></br>Chargement...',
    content: 'Chargement',
    animation: 'fade-in',
    showBackdrop: true,
    maxWidth: 200,
    showDelay: 0
  });

  $timeout(function () {
    $ionicLoading.hide();
    $scope.cocktails = Cocktails.all();
  }, 500);  

  $scope.cocktails = [];
  $scope.numberOfItemsToDisplay = 20;
  $scope.clearSearch = function() {
    this.searchFilter.nom = "";
  } 

  // ScrollInfinite : charge 10 cocktails de plus a chaque fois que l'on scroll la page
  $scope.addMoreItem = function() {
    if($scope.cocktails){
      if ($scope.cocktails.length > $scope.numberOfItemsToDisplay) {
        $scope.numberOfItemsToDisplay += 10; 
        $scope.$broadcast('scroll.infiniteScrollComplete');
      }
    }
    $scope.$broadcast('scroll.infiniteScrollComplete');
  }
});