angular.module('starter.controllers')

//utilisé pour la prise de photo
.config(function($compileProvider){
  $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel):/);
})

.controller('CreerCtrl', function($scope, Camera,Cocktails,Ingredients,Categories,$ionicPopup,$cordovaImagePicker,$http,$ionicHistory) {

  var template;
  //données de l'utilisateur
  $scope.allIngredients = {};
  $scope.ListeIngredientsHTML = "";
  $scope.ListeAjouteHTML;
  $scope.Unite = "ml";
  $scope.ingredientEnCours;
  $scope.numberOfItemsToDisplay = 50;
  $scope.QuantiteSelectionne;
  $scope.allCategorie = Categories.allCategories();
  $scope.lastPhoto = "img/categorie/Mes Cocktails.png";
  $scope.AllCocktails = Cocktails.all();

  //données à rentrer dans le JSON
  var nom;
  var idCocktail = Ingredients.getId();
  $scope.URL = '#/cocktail/' + idCocktail;
  var idCategorie;
  var masse;
  $scope.ingredients = [];
  $scope.isenable = false;
  var NewCocktail = [];
  var preparation;
  dateModification = new Date();
  var image = ' ';
  var ingredientsJSON = [];

  //reinitialisation de la page
  $scope.$on('$ionicView.leave', function(){
    $ionicHistory.clearCache();
  });

  //les différentes unités de la quantité
  $scope.clientSideList = [
    { text: "Millilitre", value: "ml" },
    { text: "Gramme", value: "g" },
    { text: "Nombre", value: "nb" },
  ];
  
  //donnees pour la quantité  
  $scope.serverSideChange = function(item) {
    $scope.Unite = item.value;
  };

  //recevoir la quantite de l'utilisateur
  $scope.getQuantite = function($event,quantite){
    $scope.QuantiteSelectionne = quantite;
  };

  //choisir l"unité
  $scope.showPopup = function() {
    $scope.data = {};
    // An elaborate, custom popup
    var myPopup = $ionicPopup.alert({
      template:  '<ion-radio ng-repeat="item in clientSideList" ng-value="item.value" ng-model="data.clientSide" ng-change="serverSideChange(item)"> {{ item.text }} </ion-radio> ',
      title: 'Choisissez l\'unité adequat',
      scope: $scope,
    });
  };

  //recevoir le nom du cokctail
  $scope.getNom = function($event,text){
    nom = text;
  };

  //recevoir l'id de la categorie choisie
  $scope.getCategorieId = function(id)
  {
    idCategorie = id;
  };

  //recevoir l'ingredient écrit par l'utilisateur
  $scope.getIngredient = function($event,ingredient){
    template = "";
    ingredient = ingredient.toLowerCase();
    $scope.ListeIngredientsHTML = "";
    $scope.allIngredients = Ingredients.getIngredients(ingredient);
    $scope.ingredientEnCours = ingredient;

    if($scope.allIngredients.length != 0)
    {
      for(var i =0;i<$scope.allIngredients.length;i++)
      {
        template = template 
          + ' <ion-list><div class="button-bar"><a class="button" on-tap = "onTap('+ i +')">' 
          +     $scope.allIngredients[i] 
          + ' </a></div></ion-list>';      
      } 
      $scope.ListeIngredientsHTML = template;
     }
  };

  $scope.getPreparation = function($event,text){
    preparation = text;    
  };

  //recevoir l'ingredient selectionné par l'utilisateur
  $scope.onTap = function(i)   
  { 
    $scope.ListeIngredientsHTML = "";
    $scope.ingredientEnCours = $scope.allIngredients[i];
    this.Ingredient = $scope.ingredientEnCours;
  };

  //vider la liste d'ingredient qui est affiché
  $scope.viderListe = function()   
  { 
    $scope.ListeIngredientsHTML = "";
  };

  //Ajouter un ingredient
  $scope.AjouterIngredient = function()
  {
    if($scope.QuantiteSelectionne > 0 && $scope.ingredientEnCours != '')
    {
      var Ajouter = true;
      for(var i = 0;i<$scope.ingredients.length;i++)
      {
        if($scope.ingredients[i*3] == $scope.ingredientEnCours)
        {
          var Ajouter = false;
        }
      }
      if(Ajouter)
      {
        $scope.ingredients.push($scope.ingredientEnCours,$scope.QuantiteSelectionne,$scope.Unite);  
        //$scope.categorie.push(idCategorie);
        update();    
        this.Ingredient = '';
        this.Quantite = '';
      }else
      {
        showAlert("Vous avez déjà ajouté cet ingredient");
      }      
    }else
    {
      console.log("ERREUR, il mnaque des données");
    }
  };

  //Supprimer un ingredient ajouté
  $scope.delete = function(index)
  {
    $scope.ingredients.splice(index, 3);
    update();
  };

  //Afficher la liste des ingredients ajoutés
  update = function () {
    $scope.ListeAjouteHTML = "";
    $scope.isenable = false;
    if($scope.ingredients.length>0)
    {
      $scope.isenable = true;
      $scope.ListeAjouteHTML = '<div class="list">';
      var localUnite = ' : '
      for(var i = 0;i < ($scope.ingredients.length / 3) ;i++)
      {
        var index = i * 3;
        localUnite = ' : '
        if($scope.ingredients[index+2] == 'nb')
        {
          localUnite = ' x ';
        }
        $scope.ListeAjouteHTML = $scope.ListeAjouteHTML 
        + '<div class="item item-button-right">'
        + $scope.ingredients[index] + localUnite + $scope.ingredients[index+1];

        if(localUnite == ' : ')
        {
          $scope.ListeAjouteHTML = $scope.ListeAjouteHTML + $scope.ingredients[index+2];
        }

        $scope.ListeAjouteHTML =  $scope.ListeAjouteHTML
        + '<button class="button"  on-tap = "delete('+ index +')"> <i class="ion-close-round"></i></button>'
        + '</div>';        
      }
      $scope.ListeAjouteHTML = $scope.ListeAjouteHTML +  '</div>';
    }
  };

  showAlert = function(message) {
    var alertPopup = $ionicPopup.alert({
      title: 'Attention',
      template: message
    });
  };


  //Ajouter une photo
  $scope.getPhoto = function() {
    console.log('Getting camera');
    Camera.getPicture({
      quality: 75,
      targetWidth: 320,
      targetHeight: 320,
      saveToPhotoAlbum: false
    }).then(function(imageURI) {
      console.log(imageURI);
      $scope.lastPhoto = imageURI;
    }, function(err) {
      console.err(err);
    });
  };

  //choisir une photo depuis la gallerie
  $scope.getimageaveContact = function() {       
    // Image picker will load image according to these settings
    var options = {
        maximumimageCount: 1, // Max number of selected image, I'm using only one for this example
        width: 800,
        height: 800,
        quality: 80            // Higher is better
    };
 
    $cordovaImagePicker.getPictures(options).then(function (results) {
        // Loop through acquired image
        for (var i = 0; i < results.length; i++) {
          console.log('Image URI: ' + results[i]);   // Print image URI
        }
    }, function(error) {
        console.log('Error: ' + JSON.stringify(error));    // In case of error
    });
  };

  //Ajouter un cocktail
  $scope.AjouterCocktail = function()
  {   
    var Ajouter = true;
    for(var i = 0;i<$scope.AllCocktails.length;i++)
    {
      if(nom == $scope.AllCocktails[i].nom)
      {
        Ajouter = false;
      }
    }

    if(Ajouter)
    {
    
      if($scope.ingredients.length>0)
      {
        masse = 0;
        for(var i = 0;i < ($scope.ingredients.length / 3) ;i++)
        {
            var index = i * 3;
            if($scope.ingredients[index+2]!='nb')
            {
              masse = masse + $scope.ingredients[index+1];
            }
        }

        for(var i = 0;i < ($scope.ingredients.length / 3) ;i++)
        {
          var index = i*3;
          var masseJson = masse;
          if($scope.ingredients[index+2]=='nb')
          {
            masseJson = 100;
          }
          var dosage = "" + ($scope.ingredients[index+1] / masseJson) * 100;
          ingredientsJSON.push(         
            {
              "nom" : $scope.ingredients[index],
              "dosage" : dosage
            }
          )
        };

        newCocktail = 
        {
          "nom" : nom,
          "idCocktail" : idCocktail,
          "masse" : masse,
          "categorie" : idCategorie,
          "ingredients" : ingredientsJSON,
          "favoris":0,
          "image_urls": 
          [
            $scope.lastPhoto
          ], 
          "preparation": preparation,
          "dateDeModification": dateModification,
          "images": 
          [
            {
              "url": $scope.lastPhoto, 
              "path":  $scope.lastPhoto, 
              "checksum": ""
            }
          ]
        };
        Cocktails.add(newCocktail);
        $scope.URL = '#/menu/cocktail/' + idCocktail;
        
      }else
      {
        showAlert('Vous avez oubliez de rentrer des ingrédients');
        $scope.URL = '#/menu/creer-cocktail'
      }
    }else
    {
      showAlert('Ce nom de cokctail existe déjà');
      $scope.URL = '#/menu/creer-cocktail'
    }
  };
})

//pouvoir comiler du langage HTMl depuis le controller (utilsé seulement si l'on utilise des événements dans notre html)
.directive('compile', ['$compile', function ($compile) {
    return function(scope, element, attrs) {
      scope.$watch(
        function(scope) {
          // watch the 'compile' expression for changes
          return scope.$eval(attrs.compile);
        },
        function(value) {
          // when the 'compile' expression changes
          // assign it into the current DOM
          element.html(value);

          // compile the new DOM and link it to the current
          // scope.
          // NOTE: we only compile .childNodes so that
          // we don't get into infinite loop compiling ourselves
          $compile(element.contents())(scope);
        }
    );
  };
}]);
