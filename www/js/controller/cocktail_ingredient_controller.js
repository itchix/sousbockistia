angular.module('starter.controllers')

/**
* Ingredient controller
* return : 
*   - $scope.categories : recupere tous les categories
**/
.controller('RechercheIngredientCtrl', function($scope, $ionicLoading, $timeout, Ingredients){

	$scope.ingModel = null;
	$scope.ingredients = [];
	$scope.cocktails = [];
	$scope.list = Ingredients.all().sort();
	$scope.numberOfItemsToDisplay = 10;

	$scope.ajoutIngredient = function(newValue, oldValue) {
		var obj = new Object();
	   	obj.nom = newValue;
	   	var jsonString = JSON.stringify(obj);
    	$scope.ingredients.push(obj);
    	$scope.rechercherParIngredient();
    };

    $scope.supprimer = function(nom) {
    	var index = $scope.ingredients.indexOf(nom);
    	if (index > -1) {
		    $scope.ingredients.splice(index, 1);
		}
		$scope.$apply();
    };

    $scope.rechercherParIngredient = function() {
    	$scope.cocktails = Ingredients.getCocktailsByIngredients($scope.ingredients);
    };

    // ScrollInfinite : charge 5 cocktails de plus a chaque fois que l'on scroll la page
	$scope.addMoreItem = function() {
		if($scope.cocktails){
		  	if ($scope.cocktails.length > $scope.numberOfItemsToDisplay) {
		    	$scope.numberOfItemsToDisplay += 5; 
		    	$scope.$broadcast('scroll.infiniteScrollComplete');
		  	}
		}
		$scope.$broadcast('scroll.infiniteScrollComplete');
	}
});