angular.module('starter.controllers')

/**
* Preparer controller
* return : 
*   - $scope.cocktail : recupere le cocktail
*   - $scope.masseActuelle : la masse actuelle du sous bock
*   - $scope.ingredientActuel : l'ingredient actuel qui est en train d'être traité
**/
.controller('PreparerCtrl', function($scope, $stateParams, $ionicPopup, $cordovaToast, Bluetooth, Cocktails, $ionicPlatform, $ionicLoading, $ionicHistory, $timeout){
  
  $scope.nbI = 0;
  $scope.cocktail = Cocktails.get($stateParams.id);
  $scope.masseActuelle = 0.0;
  $scope.masseIngredient = 0.0;
  $scope.tailleIngredients = $scope.cocktail.ingredients.length;
  $scope.ingredientActuel = $scope.cocktail.ingredients[$scope.nbI];
  $scope.nextIngredient = function () {

        if($scope.nbI  >= $scope.tailleIngredients - 1) {
            console.log("ok");
            return "c'était le dernier !";
          } else {
            console.log($scope.cocktail.ingredients[$scope.nbI+1].nom);
            return $scope.cocktail.ingredients[$scope.nbI+1].nom;
          }
  };
  $scope.cancel = function() { 
    $ionicLoading.hide();
    $ionicHistory.goBack();
  };

  $ionicHistory.currentView($ionicHistory.backView());

  $ionicLoading.show({
    template: '<ion-spinner icon="lines"></ion-spinner></br>Chargement...<br><button class="button button-clear button-dark icon-left ion-close-circled" ng-click="cancel()">Annuler</button>',
    content: 'Chargement',
    animation: 'fade-in',
    scope: $scope,
    showBackdrop: true,
    maxWidth: 200,
    showDelay: 0
  });

  $scope.$watch('ingredientActuel',function(newVal,oldVal){
    $scope.ingredientActuel = newVal;
    $scope.nextIngredient();
  });

  $scope.$on('$ionicView.leave', function(){
    bluetoothSerial.disconnect(function(response) {}, function(response) {});
    bluetoothSerial.unsubscribe(function(response) {}, function(response) {});
  });

  document.addEventListener("deviceready", function () {

    bluetoothSerial.isEnabled(function(response) {
        isConnected();
      }, function(response) {
        $ionicPopup.alert({
            title: 'Bluetooth',
            template: 'Le bluetooth est désactivé. Veuillez l\'activer'
          });
        $ionicLoading.hide();
        $ionicHistory.goBack();
      });

    function isConnected() {
      bluetoothSerial.isConnected(function(response) {
        $ionicPopup.alert({
          title: 'Connexion',
          template: 'Le sous-bock est déjà connecté.'
        });
        $ionicLoading.hide();
        $ionicHistory.goBack();
      } , function(response) {
        connect();
      });
    };

    function connect() {
      var adresse = Bluetooth.get().id;
      bluetoothSerial.connect(adresse, function(response) {
        $ionicPopup.alert({
            title: 'Connexion',
            template: 'Connecté au sous bock ayant comme adresse mac : ' + adresse
          });
        bluetoothSerial.subscribe("\n", function(response) {
            affiche(response);
          }, function(response) {
            afficheErr(response);
          });
        $scope.write("init\n");
        $scope.write("masseCocktail " + $scope.cocktail.masse + "\n");
        $ionicLoading.hide();
      }, function(response) {
        $ionicPopup.alert({
            title: 'Connexion',
            template: 'Erreur de connexion au sous bock ayant comme adresse mac : ' + adresse
          });
        $ionicLoading.hide();
        $ionicHistory.goBack();
      });
    };

    $scope.write = function(data) {
      bluetoothSerial.write(data, function(response) {
        console.log("Write : " + data);
      });
    };

    function affiche(response) {
        console.log("Read : " + response);
        if(response.trim() == "Masse_reçue") {
          $scope.write("dosageIngredient " + $scope.ingredientActuel.dosage + "\n");
          $scope.masseIngredient = $scope.ingredientActuel.dosage/100 * $scope.cocktail.masse;
        } else if(response.trim() == "Stop_ingredient") {
          if($scope.nbI >= $scope.tailleIngredients - 1) {
            $scope.nbI++;
            $scope.write("Cocktailtermine\n");
          } else {
            $scope.nbI++;
            $scope.ingredientActuel = $scope.cocktail.ingredients[$scope.nbI];
            $scope.write("dosageIngredient " + $scope.ingredientActuel.dosage + "\n");
          }
        } else if(response.trim() == "Cocktailtermine_ok") {
          bluetoothSerial.unsubscribe();
          $ionicPopup.alert({
            title: 'Fini!',
            template: 'Vous avez fini votre cocktail!'
          });
        } else if(response.trim() == "Dosageingredient_recu") {
          if($scope.masseActuelle >= $scope.ingredientActuel.dosage) {
            $timeout(function () {
              // Do nothing
            }, 500);  
          }
          $scope.write("masseActuelle\n");
        }else if(response.trim().indexOf("masse") > -1) {
          $scope.write("masseActuelle\n");
          $scope.masseActuelle = response.trim().substring(6);
          $scope.$apply();
        }
    };


    function afficheErr(response) {
      $cordovaToast
          .show('Erreur données reçues : ' + data, 'short', 'bottom')
          .then(function(success) {
            // success
          }, function (error) {
            // error
        });
    };
  }, false);

  /* **********************************
  * Gestion de la "gauge"  nous permettant de voir l'avancé de la préparation
  */
  $scope.max =            50;
  $scope.offset =         0;
  $scope.timerCurrent =   0;
  $scope.uploadCurrent =  0;
  $scope.stroke =         15;
  $scope.radius =         125;
  $scope.isSemi =         false;
  $scope.rounded =        false;
  $scope.responsive =     false;
  $scope.clockwise =      true;
  $scope.currentColor =   '#45ccce';
  $scope.bgColor =        '#eaeaea';
  $scope.duration =       800;
  $scope.currentAnimation = 'easeOutCubic';
  $scope.animationDelay = 0;

});