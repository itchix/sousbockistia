angular.module('starter.controllers')

/**
* Home controller
* return : 
*   - $scope.cocktails : recupere les cocktails
**/
.controller('HomeCtrl', function($scope, Cocktails) {
  $scope.cocktails = Cocktails.all();

});