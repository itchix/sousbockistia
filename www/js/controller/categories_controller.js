angular.module('starter.controllers')

/**
* Categories controller
* return : 
*   - $scope.categories : recupere tous les categories
**/
.controller('CategoriesCtrl', function($scope, Categories, $ionicLoading, $timeout){
	$ionicLoading.show({
	    template: '<ion-spinner icon="lines"></ion-spinner></br>Chargement...',
	    content: 'Chargement',
	    animation: 'fade-in',
	    showBackdrop: true,
	    maxWidth: 200,
	    showDelay: 0
	});

	$timeout(function () {
	    $ionicLoading.hide();
	    $scope.categories = Categories.allCategories();
	}, 500); 

	$scope.categories = [];
	$scope.numberOfItemsToDisplay = 20;
	$scope.clearSearch = function() {
		this.searchFilter.nom = "";
	};

	// ScrollInfinite : charge 10 cocktails de plus a chaque fois que l'on scroll la page
  	$scope.addMoreItem = function() {
    if($scope.cocktails){
      	if ($scope.cocktails.length > $scope.numberOfItemsToDisplay) {
        	$scope.numberOfItemsToDisplay += 10; 
        	$scope.$broadcast('scroll.infiniteScrollComplete');
      	}
    }
    $scope.$broadcast('scroll.infiniteScrollComplete');
  }
});