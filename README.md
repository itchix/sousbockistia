## Plugins 
 
- Pour le splashscreen : 
```bash
$ cordova plugin add cordova-plugin-splashscreen
```

- Pour le bluetooth : 
```bash
$ cordova plugin add cordova-plugin-bluetooth-serial
```

- Pour le toast : 
```bash
$ cordova plugin add https://github.com/EddyVerbruggen/Toast-PhoneGap-Plugin.git
```

- Pour la camera : 
```bash
$ cordova plugin add cordova-plugin-camera
```

## Installation 

### Android
Génération de l'appli pour Android: 
```bash
$ ionic platform add android
$ ionic build android
$ ionic emulate android
$ ionic run android --device
```
Pour récupérer l'apk il faut aller ici : "/platforms/android/build/outputs/apk/android-debug.apk"

Pour debugger sur android : 
```bash
$ adb logcat chromium:D SystemWebViewClient:D  BluetoothSerial:D BluetoothSerialService:D *:S
```

### IOS
Génération de l'appli pour IOS: 
```bash
$ ionic platform add ios
$ ionic build ios
$ ionic emulate ios
```

### Web
Pour le lancer sur un navigateur web:
```bash
$ ionic serve
```
